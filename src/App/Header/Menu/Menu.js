import React from 'react' 
import { Link } from 'react-router-dom'

import Guide from './../../../common/style/images/Guide.png'
import about from './../../../common/style/images/about.png'
import thetour from './../../../common/style/images/thetour.png'
import Howto from './../../../common/style/images/Howto.png'


import './menu.css'

const Menu = () => {
    return (
            <nav>
				<ul className="menu">
					<li className="mainPage"><Link to="/"><img src={Guide} alt=""/><p>Главная</p></Link></li>
					<li className="europe"><Link to="/Europe"><img src={thetour} alt=""/><p>Европа</p></Link></li>
					<li className="africa"><Link to="/Africa"><img src={about} alt=""/><p>Африка</p></Link></li>
					<li className="america"><Link to="/America"><img src={thetour} alt=""/><p>Америка</p></Link></li>	
					<li className="asia"><Link to="/Asia"><img src={Howto} alt=""/><p>Азия</p></Link></li>
				</ul>
			</nav>
    )
}

export default Menu