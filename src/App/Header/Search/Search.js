import React from 'react' 

import './search.css'

const Search = () => {
    return (
        <div className="search flex-head">
            <form action="" className="form">
                <input className="search" type="text" placeholder="Search for somthing here"/>
                <input className="search_clock" type="submit" value=" "/>
            </form>
        </div>
    )
}

export default Search