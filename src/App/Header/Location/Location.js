import React from 'react' 
import mappin from './../../../common/style/images/mappin.png'

import './location.css'

const Location = () => {
    return (
        <div className="location-flex">
            <div className="location flex-head">
                <p>Quicklinks</p>
                <a href="/">
                    <img src={mappin} alt=""/>
                </a>
            </div>
            <div className="location_select flex-head">
                <p>Select from dropdown</p>
            </div>
        </div>
    )
}

export default Location