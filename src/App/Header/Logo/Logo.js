import React from 'react' 
import logo from './../../../common/style/images/logo.png'

import './logo.css'

const Logo = () => {
    return (
        <div className="logo">
            <a href="/">
                <img src={logo} alt=""/>
            </a>
            <div>
            <p className="logotext">travler</p>
            <p className="logosubtext">Get the advice you need about travel</p>
            </div>	
        </div>
    )
}

export default Logo