import React from 'react'

import './header.css'

import Logo from './Logo/Logo'
import Location from './Location/Location'
import Search from './Search/Search'
import Menu from './Menu/Menu'

const Header = () => {
	return(
		<header className="header">
            <div className="container">
                <div className="travler">
                    <Logo/>
                    <Location/>
                    <Search/>
                </div>
                <Menu/>  
            </div>
		</header>
	)
}

export default Header