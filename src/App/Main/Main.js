import React, { Component } from 'react'
import { Route } from 'react-router-dom'

import MainSection1 from './MainSection1/MainSection1'
import MainSection2 from './MainSection2/MainSection2'
import MainSection3 from './MainSection3/MainSection3'
import Africa from './Africa/Africa'
import Europe from './Europe/Europe'
import America from './America/America'
import Asia from './Asia/Asia'
import ArticlePage from '../../Components/ArticlePage'
import Search from './Europe/Search'
import Liked from './MainSection3/Liked'

class Main extends Component {
	
	state = {
		value: ""
	};

	handleChange = this.handleChange.bind(this);
  
	handleChange(event) {
		this.setState({value: event.target.value});
  	}

	render(){
		const{
			isLiked,
			addLike,
			removeLike,
		}=this.props
	return(
		<main>
			<Route exact path="/" component={MainSection1}/>
			<Route exact path="/" render={()=>(
				<MainSection3
					isLiked={isLiked}
					addLike={addLike}
				removeLike={removeLike}
				/>
					)}/>
			<Route exact path="/" component={MainSection2}/>
			<Route path="/Africa" component={Africa}/>
			<Route path="/Europe" render={()=>(
					<Europe
						handleChange={this.handleChange}
						value={this.state.value}
					/>
					)}/>
			<Route path="/America" component={America}/>
			<Route path="/Asia" component={Asia}/>
			<Route path="/liked"render={()=>(
				<Liked
				isLiked={isLiked}
				/>
				)}/>
			<Route path="/article/:articleId" component={ArticlePage}/>
			<Route path="/search" render={()=>(
					<Search
						value={this.state.value}
					/>
				)}/>
		</main>
	)}
}

export default Main