import React from "react"

import './africa.css'

import countryData from './../MainSection3/countryData'
import BlogRow from './../MainSection3/BlogRow/BlogRow'
import Category from './../MainSection3/Category'

const Africa = () => {

    return(
        <div>
            <div className="africaPage"></div>
            <div className="mainSection3Page">
                <div className="container">
                     <Category/>
                    <h1 className="titlePage">Африка</h1>
                    <div className="mainRows">
                        {
                            countryData.filter(country => country.category==="Африка").map(({
                                id,
                                country,
                                text
                            })=>(   
                                <BlogRow 
                                key={id}
                                id={id}
                                country={country}
                                text={text}
                                />
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Africa