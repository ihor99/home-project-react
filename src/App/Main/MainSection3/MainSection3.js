import React from 'react'

import './mainSection3.css'

import countryData from './countryData'
import BlogRow from './BlogRow/BlogRow'
import OurTeam from './OurTeam/OurTeam'
import Category from './Category'


const MainSection3 = ({
	addLike,
	removeLike
}) => {
	
	return(
		<div className="mainSection3">
			<div className="container">
				<OurTeam/>
				<Category/>
				<div className="mainRows">
				{
					countryData.map(({
						id,
						country,
						prevText
					  })=>(   
						
							<BlogRow 
							key={id}
							id={id}
							country={country}
							prevText={prevText}
							addLike={addLike}
							removeLike={removeLike}
							/>
							
					  ))  
				}
				
				</div>
			</div>
		</div>
	)
}

export default MainSection3