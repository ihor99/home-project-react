import React, { Component } from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import './blogRow.css'


class BlogRow extends Component{

    changeLikeState(){
        const{
            isLiked,
            addLike,
            removeLike,
            id,
            addProductToCart      
        }=this.props
        isLiked ? removeLike(id) : addLike(id)
        addProductToCart(id)
    }

render(){
  
    const{
        country,
        text,
        prevText,
        id,
        isLiked,

    } = this.props;

    return(
            <div className="blogrowtext blogSection3">
                   <Link className="titleArticle" to={`/article/${id}`}>{country}</Link>
                <p>{text}{prevText}</p>
        <button className="addtext" onClick={()=>this.changeLikeState()}>
        {
        isLiked?<span>&#9829;</span>:<span>&#9825;</span>
        }
        
        </button>
            </div>
	) 
}
}

const mapStateToProps = (state,props) => ({
    isLiked:state.likeButtonsState[props.id]
})
const mapDispatchToProps = (dispatch) => ({
    addLike:(id)=>dispatch({
        type:"LIKE",
        id:id
    }),
    removeLike:(id)=>dispatch({
        type:"DISLIKE",
        id:id
    }), 
    addProductToCart:(id)=>dispatch({
        type: "ADD_PRODUCT_TO_CART",
        id:id,
    })

 
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BlogRow)
