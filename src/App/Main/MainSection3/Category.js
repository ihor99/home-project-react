import React from "react"
import {Link} from 'react-router-dom'

import './mainSection3.css'

const Category = () => {

    return(
        <div className="category">
            <div className="region"><Link to="/">Главная</Link></div>
            <div><hr className="hrRegion"/></div>
            <div className="region"><Link to="/Europe">Европа</Link></div>
            <div><hr className="hrRegion"/></div>
            <div className="region"><Link  to="/Asia">Азия</Link></div>
            <div><hr className="hrRegion"/></div>
            <div className="region"><Link  to="/Africa">Африка</Link></div>
            <div><hr className="hrRegion"/></div>
            <div className="region"><Link  to="/America">Америка</Link></div>
            <div><hr className="hrRegion"/></div>
            <div className="region"><Link  to="/liked">Вибрані</Link></div>
        </div>
    )
}

export default Category