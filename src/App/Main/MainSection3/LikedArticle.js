import React, { Component } from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import './BlogRow/blogRow.css'


class LikedArticle extends Component{

render(){
  
    const{
        countrys,
        isLiked,
        id
    } = this.props;
    return(
           
           <div className="blogrowtext blogSection3">
                   <Link className="titleArticle" to={`/article/${id}`}>{countrys.country}</Link>
                <p>{countrys.text}{countrys.prevText}</p>
                <button className="addtext">
                {
                isLiked?<span>&#9829;</span>:<span>&#9825;</span>
                }
                
        </button>
            </div>
	) 
}
}

const mapStateToProps = (state,props) => ({
    isLiked:state.likeButtonsState[props.id]
})

export default connect(
    mapStateToProps
)(LikedArticle)
