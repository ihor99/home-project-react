import React from "react"
import {connect} from 'react-redux'
import './../Europe/europe.css'
import {keys} from 'lodash'
import countryData, {getArticlesMap} from './countryData'
import LikedArticle from './LikedArticle'

const Liked = ({
    productsInCart,
    countryObj =getArticlesMap(countryData),
}) => {
   
    return(
        <div>
            <div className="mainSection3Page">
                <div className="container">
                    <div className='likedArticle'>Статті що лайкнули</div>
                    <div className="mainRows">
                        {
                             
                            keys(productsInCart).map((productId)=>(   
                                <LikedArticle 
                                productsInCart={productsInCart}
                                key={productId}
                                id={productId}
                                countrys={countryObj[productId]}
                                />
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state) => ({
    productsInCart:state.productsInCart
})

export default connect(
    mapStateToProps
)(Liked)