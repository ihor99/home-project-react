import React, { useState, useEffect } from 'react'
import Swiper from 'react-id-swiper';
import 'swiper/css/swiper.css'

import css from './ourTeam.css';
import data from './slideData';

import Slide from './Slide'

const OurTeam = () => {
    const [swiper, updateSwiper] = useState(null);
    const [translate, updateTranslate] = useState(0);
    const [transition, updateTransition] = useState(0);

    const params = {
        slidesPerView: 3,
      };

    useEffect(() => {
    if (swiper) {
        swiper.on('setTranslate', (t) => {
        updateTranslate(t);
        });
        swiper.on('setTransition', (t) => {
        updateTransition(t);
        });
    }
    }, [swiper]);
	return(
            <div>
                <div className="ourTeam">
                        <div><hr/></div>
                        <div><p className="titleOurTeam">A humble Team we are</p></div>
                        <div><hr/></div>
                    </div>
                    <div className="subOurTeam"><p>We’ll some of us anyways</p></div>
                    <div className="wrap">
                    <div className={css.slider}>
                    <Swiper getSwiper={updateSwiper} {...params}>
                    {data.map((item, idx) => (
                        <div key={idx}>
                        <Slide
                            translate={translate}
                            transition={transition}
                            photo={item.photo}
                            name={item.name}
                            profession={item.profession}
                            >

                           
                            
                        </Slide>
                        </div>
                    ))}
                    </Swiper></div>
                    </div>
            </div>
	)
}

export default OurTeam