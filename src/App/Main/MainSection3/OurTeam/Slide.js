import React,{ useRef, useState, useEffect } from 'react';


import './our.css'

const Slide = ({ 
  translate, 
  transition, 
  photo,
  name,
  profession
}) => {
    const container = useRef(null);
    const [offsetLeft, updateOffsetLeft] = useState(0);
    const [width, updateWidth] = useState(1);
  
    useEffect(() => {
      setTimeout(() => {
        const parents = container.current.parentElement;
        updateOffsetLeft(parents.offsetLeft);
        updateWidth(parents.offsetWidth);
      }, 0);
    }, []);
  
    const x = -translate - offsetLeft;
    const k = 1 - x / width; // [0 : 1]
  
    const style = x >= -1 ? {
      transform: `translateX(${x}px) scale(${k * 0.2 + 0.8})`, // [0.8 : 1]
      opacity: k < 0 ? 0 : k * 0.5 + 0.5, // [0.5 : 1]
      transition: `${transition}ms`,
    } : {};
  
  return (
    <div ref={container} style={style} >
      <div className="backgroundPhoto">
        <img src={photo} alt=""/> 
        <p className="photoName">{name}</p>
        <p className="photoProf">{profession}</p>
      </div>
    </div>
  );
};

export default Slide;