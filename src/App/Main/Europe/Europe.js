import React, { Component } from "react"

import './europe.css'

import countryData from './../MainSection3/countryData'
import BlogRow from './../MainSection3/BlogRow/BlogRow'
import Category from './../MainSection3/Category'
import { Link } from "react-router-dom"

class Europe extends Component {

    render(){
        const {
            handleChange,
            value
        } = this.props
        return(
            <div>
                <div className="europePage"></div>
                <div className="mainSection3Page">
                    <div className="container">
                        <Category/>
                        <form >
                        <input type="search" value={value} onChange={handleChange} ></input>
                        <Link to="/search" ><input type="submit" value="Отправить" /></Link>
                        </form>
                        
                        <h1 className="titlePage">Европа</h1>
                        <div className="mainRows">
                            {
                                countryData.filter(country => country.category==="Европа").map(({
                                    id,
                                    country,
                                    text
                                })=>(   
                                    <BlogRow 
                                    key={id}
                                    id={id}
                                    country={country}
                                    text={text}
                                    />
                                ))
                            }
                        </div>
                    </div>
                </div>
            </div>
        )}
}

export default Europe