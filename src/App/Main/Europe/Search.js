import React from "react"

import './europe.css'

import countryData from './../MainSection3/countryData'
import BlogRow from './../MainSection3/BlogRow/BlogRow'


const Search = ({
    value
}) => {
   
   
    return(
        <div>
            <div className="europePage"></div>
            <div className="mainSection3Page">
                <div className="container">
                    
                    <div className="mainRows">
                        {
                            countryData.filter(country => country.country===value).map(({
                                id,
                                country,
                                text
                            })=>(   
                                <BlogRow 
                                key={id}
                                id={id}
                                country={country}
                                text={text}
                                />
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Search