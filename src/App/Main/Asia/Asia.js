import React from "react"

import './asia.css'

import countryData from './../MainSection3/countryData'
import BlogRow from './../MainSection3/BlogRow/BlogRow'
import Category from './../MainSection3/Category'

const Asia = () => {

    return(
        <div>
            <div className="asiaPage"></div>
            <div className="mainSection3Page">
                <div className="container">
                     <Category/>
                    <h1 className="titlePage">Азия</h1>
                    <div className="mainRows">
                        {
                            countryData.filter(country => country.category==="Азия").map(({
                                id,
                                country,
                                text
                            })=>(   
                                <BlogRow 
                                key={id}
                                id={id}
                                country={country}
                                text={text}
                                />
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Asia