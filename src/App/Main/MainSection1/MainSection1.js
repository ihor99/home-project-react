import React from 'react'
import mainicon from './../../../common/style/images/mainicon.png'

import './mainSection1.css'

const MainSection1 = () => {
	return(
		<div className="mainSection1">
			<div className="container">
				<div className="mainicon">
					<a href="/">
						<img src={mainicon} alt=""/>
					</a>
				</div>
			</div>
		</div>
	)
}

export default MainSection1