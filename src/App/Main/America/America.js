import React from "react"

import './america.css'

import countryData from './../MainSection3/countryData'
import BlogRow from './../MainSection3/BlogRow/BlogRow'
import Category from './../MainSection3/Category'

const America = () => {

    return(
        <div>
            <div className="americaPage"></div>
            <div className="mainSection3Page">
                <div className="container">
                     <Category/>
                    <h1 className="titlePage">Америка</h1>
                    <div className="mainRows">
                        {
                            countryData.filter(country => country.category==="Америка").map(({
                                id,
                                country,
                                text
                            })=>(   
                                
                                <BlogRow 
                                key={id}
                                id={id}
                                country={country}
                                text={text}
                                />
                            
                            
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default America
