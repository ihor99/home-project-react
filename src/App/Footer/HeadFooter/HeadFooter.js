import React from 'react'
import footerlogo from './../../../common/style/images/footerlogo.png'

import './headFooter.css'

const HeadFooter = () => {
	return(
		<div className="headfooter">
            <a href="/">
                <img src={footerlogo} alt=""/>
            </a>
            <div>
            <p className="footerlogotext">travler</p>
            <p className="footerlogosubtext">Get the advice you need about travel</p>
            </div>	
            <div><p>Portfolio</p></div>
            <div><p>Services</p></div>
            <div><p>Staff</p></div>
            <div><p>Articles</p></div>
            <div><p>Contact</p></div>
        </div>
	)
}

export default HeadFooter