import React from 'react'

import './footerLowText.css'

const FooterLowText = () => {
	return(
		<div className="footerLowText">
            <div><p>About us</p></div>
            <div><p>Footer Links here</p></div>
            <div><p>Privacy</p></div>
            <div><p>Icons by http://www.dribbble.com/joshuaandrewdavies</p></div>
        </div>
	)
}

export default FooterLowText