import React from 'react'

import './footer.css'

import HeadFooter from './HeadFooter/HeadFooter'
import FooterMainText from './FooterMainText/FooterMainText'
import FooterLowText from './FooterLowText/FooterLowText'
import FooterLittleText from './FooterLittleText/FooterLittleText'

const Footer = () => {
	return(
		<footer>
			<div className="container">
				<HeadFooter/>
				<FooterMainText/>
				<FooterLowText/>
				<FooterLittleText/>
			</div>
		</footer>
	)
}

export default Footer