import React, { Component } from 'react'
import countryData, { getArticlesMap } from '../App/Main/MainSection3/countryData';

import './articlePage.css'

import Category from '../App/Main/MainSection3/Category'


class ArticlePage extends Component{
    render(){
        const{
            match,
            articleObj = getArticlesMap(countryData),
        } = this.props;

    const id = match.params.articleId;
    return (
        <div>
         <div><img src={articleObj[id].image} alt="Category"></img></div>
         <div className="mainSection3Page">
            <div className="container">
                    <center>
                        <Category/>
                        <h1 className="titleArticle">{articleObj[id].country}</h1>
                        <div className="articlePageRow blogSection3">
                            <div className="mainText"
                                dangerouslySetInnerHTML={{
                                    __html:articleObj[id].fulltext
                                }}
                            
                                >
                            </div>
                            <div className="addtext"><p>+</p></div>
                        </div>    
                    </center>
            </div>
        </div>
        </div>
        )}
}

export default ArticlePage