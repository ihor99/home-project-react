import {combineReducers} from 'redux'
import articleLikeReducer from './articleLikeReducer'
import cartReducer from './cartReducer'

const rootReducer = combineReducers({
    likeButtonsState:articleLikeReducer,
    productsInCart:cartReducer
})

export default rootReducer