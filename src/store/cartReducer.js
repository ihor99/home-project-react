

const cartReducer = (state={

},action)=>  {
    switch(action.type){
        case "ADD_PRODUCT_TO_CART":
            return {
                ...state,
                [action.id] : (state[action.id] || 0)+ 1
            }
        default:
            return state
    }
}
export default cartReducer